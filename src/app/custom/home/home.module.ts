import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {RecapComponent} from './recap/recap.component';
import {AdviserComponent} from './adviser/adviser.component';
import {SimulationComponent} from './simulation/simulation.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [HomeComponent, RecapComponent, AdviserComponent, SimulationComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule
  ]
})
export class HomeModule {
}
