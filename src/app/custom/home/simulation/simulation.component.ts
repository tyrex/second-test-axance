import {Component, OnInit} from '@angular/core';
import {SimulationService} from './services/simulation.service';

@Component({
  selector: 'app-simulation',
  templateUrl: './simulation.component.html',
  styleUrls: ['./simulation.component.scss']
})
export class SimulationComponent implements OnInit {

  simulationEmail = '';
  error: { code: number, msg: string } = {code: null, msg: ''};

  constructor(private simulationService: SimulationService) {
  }

  ngOnInit() {
  }

  submitSimulation() {
    if (this.simulationEmail.length > 0 && this.validateEmail(this.simulationEmail)) {
      this.error = {code: 2, msg: 'Chargement ...'};
      const bodyToSend = {
        title: 'Test front',
        body: this.simulationEmail,
        userId: 1
      };
      this.simulationService.addPost(bodyToSend).subscribe((result) => {
        console.log(result);

        this.error = {code: 1, msg: 'Opération réussi'};
      }, (error) => {
        this.error = {code: 0, msg: 'Erreur'};
      });
    } else {
      this.error = {code: 0, msg: 'Email incorrect'};
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

}
