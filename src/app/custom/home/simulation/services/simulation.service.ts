import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {GlobalService} from '../../../../shared/global-services/global.service';
import {environment} from '../../../../../environments/environment';

const localUrl = environment.SERVER_URL + 'posts';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class SimulationService {

  constructor(private http: HttpClient) {
  }

  addPost(post): Observable<any> {
    return this.http.post<any>(localUrl, post, httpOptions)
      .pipe(
        catchError(GlobalService.handleError('addPost', post))
      );
  }
}
