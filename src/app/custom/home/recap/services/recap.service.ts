import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {GlobalService} from '../../../../shared/global-services/global.service';
import {environment} from '../../../../../environments/environment';

const localUrl = environment.SERVER_URL + 'photos';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RecapService {


  constructor(private http: HttpClient) {
  }

  getImages(): Observable<any> {
    const stringparams = '_start=12&_limit=5';
    const params = new HttpParams({fromString: stringparams});
    const options = {params};
    return this.http.get<any[]>(localUrl, options).pipe(
      retry(3), catchError(GlobalService.handleError<any[]>('getImages', [])));
  }
}
