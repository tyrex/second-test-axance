import {Component, OnInit} from '@angular/core';
import {RecapService} from './services/recap.service';

@Component({
  selector: 'app-recap',
  templateUrl: './recap.component.html',
  styleUrls: ['./recap.component.scss']
})
export class RecapComponent implements OnInit {

  images = [];

  constructor(private recapService: RecapService) {
  }

  ngOnInit() {
    this.recapService.getImages().subscribe((result) => {
      this.images = result;
      console.log(result);
    });
  }

  activeStarsNumber(id) {
    return (id % 5) + 1;
  }

  disabledStarsNumber(id) {
    return 5 - ((id % 5) + 1);
  }

  removeImage(id) {
    this.images = this.images.filter((element) => {
      return element.id !== id;
    });
  }

}
