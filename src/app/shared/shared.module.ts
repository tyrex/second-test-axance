import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  imports: [
    HttpClientModule,
    CommonModule
  ],
  declarations: [
  ],
  exports: [
    HttpClientModule,
  ],
  providers: []
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule, providers: [
      ]
    };
  }
}
