import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor() {
  }

  static handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      GlobalService.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  static log(message: string) {
    console.log(message);
  }
}
