export const environment = {
  production: true,
  lang: ['en', 'fr'].includes(localStorage.getItem('lang')) ? localStorage.getItem('lang') : 'en',
  SERVER_URL: 'https://jsonplaceholder.typicode.com/',
  cached: false
};
