FROM node:13-alpine

# If you have native dependencies, add these extra tools
# RUN apk add --no-cache make gcc g++ python
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global


RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app


# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json ./

RUN npm -g config set user root
RUN npm install

COPY . .

EXPOSE 4200

CMD [ "npm", "start" ]

