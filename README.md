# Second Axance Test

## Docker deployment (Recommended option)
You must first have docker installed on your machine.

- Run `docker-compose build`
- Run `docker-compose up`
### Dev version
- Open 'http://localhost:4200' in browser

### PROD version
- Open 'http://localhost' in browser

## Local deployment
You must have the below tools installed
 - Node 12 (https://nodejs.org/en/blog/release/v12.13.0/)
 - Angular CLI (npm install -g @angular/cli@8)
 
Start FrontEnd server
 - Go to root folder
 - Run `npm install`
 - Run `npm start` (To run server on default port 4200)
