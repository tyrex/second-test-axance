FROM node:13-alpine as builder
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app


# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json ./

RUN npm -g config set user root
RUN npm install

COPY . .

RUN npm run build:prod

FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /usr/src/app/dist/test-axance-second /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
